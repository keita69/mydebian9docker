FROM debian:stretch

ENV MYDEBIAN9DOCKER_VERSION build-target
ENV MYDEBIAN9DOCKER_VERSION latest
ENV MYDEBIAN9DOCKER_VERSION stable
ENV MYDEBIAN9DOCKER_IMAGE mydebian9docker

# set locale
RUN apt-get update && \
    apt-get install -y locales  apt-transport-https  ca-certificates  sudo  curl  gnupg2   software-properties-common && \
    localedef -i ja_JP -c -f UTF-8 -A /usr/share/locale/locale.alias ja_JP.UTF-8 && \
    apt-get clean
ENV LANG ja_JP.utf8

# install man pages
RUN apt-get install -y man-db/stable  manpages/stable && apt-get clean

# install etc utils
RUN apt-get install -y \
        git \
        jq \
        ansible \
        bash-completion \
        dnsutils \
        expect \
        gettext \
        make \
        netcat \
        wget \
        vim \
        traceroute \
        tree \
        zip \
        unzip \
        libssl-dev \
        libffi-dev \
        python-dev \
        python-pip \
    && apt-get clean all

# install docker client
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
    add-apt-repository  "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" && \
    apt-get update && \
    apt-get install -y docker-ce-cli/stretch && apt-get clean

# install docker-compose
RUN apt-get install -y docker-compose/stable && apt-get clean

# install kubectl CLI
RUN curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" >/etc/apt/sources.list.d/kubernetes.list && \
    apt-get update && \
    apt-get install -y kubectl && apt-get clean
# RUN apt-mark hold kubectl

# install helm CLI v2.9.1
ENV HELM_CLIENT_VERSION v2.9.1
RUN curl -LO https://storage.googleapis.com/kubernetes-helm/helm-${HELM_CLIENT_VERSION}-linux-amd64.tar.gz && \
    tar xzf  helm-${HELM_CLIENT_VERSION}-linux-amd64.tar.gz && \
    /bin/cp  linux-amd64/helm   /usr/bin && \
    /bin/rm -rf rm helm-${HELM_CLIENT_VERSION}-linux-amd64.tar.gz linux-amd64

# install kompose v1.17.0
ENV KOMPOSE_VERSION v1.17.0
RUN curl -LO https://github.com/kubernetes/kompose/releases/download/${KOMPOSE_VERSION}/kompose-linux-amd64.tar.gz && \
    tar xzf kompose-linux-amd64.tar.gz && \
    chmod +x kompose-linux-amd64 && \
    mv kompose-linux-amd64 /usr/bin/kompose && \
    rm kompose-linux-amd64.tar.gz

# install stern
ENV STERN_VERSION 1.10.0
RUN curl -LO https://github.com/wercker/stern/releases/download/${STERN_VERSION}/stern_linux_amd64 && \
    chmod +x stern_linux_amd64 && \
    mv stern_linux_amd64 /usr/bin/stern

# install kustomize
ENV KUSTOMIZE_VERSION 1.0.11
RUN curl -LO https://github.com/kubernetes-sigs/kustomize/releases/download/v${KUSTOMIZE_VERSION}/kustomize_${KUSTOMIZE_VERSION}_linux_amd64 && \
    chmod +x kustomize_${KUSTOMIZE_VERSION}_linux_amd64 && \
    mv kustomize_${KUSTOMIZE_VERSION}_linux_amd64 /usr/bin/kustomize

# install kubectx, kubens. see https://github.com/ahmetb/kubectx
RUN curl -LO https://raw.githubusercontent.com/ahmetb/kubectx/master/kubectx && \
    curl -LO https://raw.githubusercontent.com/ahmetb/kubectx/master/kubens && \
    chmod +x kubectx kubens && \
    mv kubectx kubens /usr/local/bin

# install kubectl-aliases. see https://github.com/ahmetb/kubectl-aliases
RUN git clone https://github.com/ahmetb/kubectl-aliases.git && \
    mv ./kubectl-aliases/.kubectl_aliases ~/ && \
    rm -rf ./kubectl-aliases 

# install yamlsort
ENV YAMLSORT_VERSION v0.1.14
RUN curl -LO https://github.com/george-pon/yamlsort/releases/download/${YAMLSORT_VERSION}/linux_amd64_yamlsort_${YAMLSORT_VERSION}.tar.gz && \
    tar xzf linux_amd64_yamlsort_${YAMLSORT_VERSION}.tar.gz && \
    chmod +x linux_amd64_yamlsort && \
    mv linux_amd64_yamlsort /usr/bin/yamlsort && \
    rm linux_amd64_yamlsort_${YAMLSORT_VERSION}.tar.gz

RUN echo "=================================================================="
RUN echo "== INSTALL AZ-CLI  "
RUN echo "=================================================================="

# install Azure tools(az-cli, az-ansible)
RUN sudo apt-get install apt-transport-https lsb-release software-properties-common dirmngr -y && \
    AZ_REPO=$(lsb_release -cs) && \
    echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $AZ_REPO main" | \
    sudo tee /etc/apt/sources.list.d/azure-cli.list && \
    sudo apt-key --keyring /etc/apt/trusted.gpg.d/Microsoft.gpg adv \
         --keyserver packages.microsoft.com \
         --recv-keys BC528686B50D79E339D3721CEB3E94ADBE1229CF && \
    sudo apt-get update && \
    sudo apt-get install azure-cli && \
    pip install 'ansible[azure]'

ADD docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ADD bashrc /root/.bashrc
ADD bash_profile /root/.bash_profile
ENV HOME /root
ENV ENV $HOME/.bashrc

CMD ["/usr/local/bin/docker-entrypoint.sh"]

